﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ArSceneController : MonoBehaviour
{
    public TextMeshProUGUI TextButtonActivate;
    public TextMeshProUGUI TextButtonRepeat;
    public TextMeshProUGUI TextButtonFinish;

    [HideInInspector]
    public LocalRepositoryController LocalRepositoryController;

    private void Awake()
    {
        LocalRepositoryController = new LocalRepositoryController();

        string language = LocalRepositoryController.GetLanguage().ToUpper();
        switch (language)
        {
            case ("RU"):
                TextButtonActivate.text = "Слушать Bugaddy";
                TextButtonRepeat.text = "Повторить";
                TextButtonFinish.text = "Закончить";
                break;
            case ("EN"):
                TextButtonActivate.text = "Listen Bugaddy";
                TextButtonRepeat.text = "Repeat";
                TextButtonFinish.text = "Finish";
                break;
            case ("ES"):
                TextButtonActivate.text = "Escucha Bugaddy";
                TextButtonRepeat.text = "Repetir";
                TextButtonFinish.text = "Finalizar";
                break;
        }
    }
}
