﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BugaddyController : MonoBehaviour
{
    public Animator Animator;
    public AudioSource AudioSource;

    public AudioClip RuAudioClip;
    public AudioClip EnAudioClip;
    public AudioClip EsAudioClip;


    private AudioClip m_AudioClip;

    void Awake()
    {
        //AudioSource.Stop();
        LocalRepositoryController localRepositoryController = new LocalRepositoryController();
        switch (localRepositoryController.GetLanguage())
        {
            case ("EN"):
                AudioSource.clip = EnAudioClip;
                break;
            case ("RU"):
                AudioSource.clip = RuAudioClip;
                break;
            case ("ES"):
                AudioSource.clip = EsAudioClip;
                break;
        }
    }

    public void Activate()
    {
        Restart();
    }

    public void Restart()
    {
        AudioSource.Stop();
        Animator.SetTrigger("active");
        AudioSource.Play();
    }
}
