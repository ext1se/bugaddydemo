﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BugaddyOldController : MonoBehaviour
{
    public GameObject CanvasSpeak;
    public GameObject Audio;

    void Start()
    {
        CanvasSpeak.SetActive(false);
        Audio.SetActive(false);
    }

    public void Activate()
    {
        CanvasSpeak.SetActive(true);
        Audio.SetActive(true);
    }
}
