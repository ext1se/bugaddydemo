﻿using Michsky.UI.ModernUIPack;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CommentController : MonoBehaviour
{
    //public const string EMAIL = "app@bugaddy.com"; //
    public const string EMAIL = "info@bugaddy.com"; //
    //public const string EMAIL = "ponomarevigor1995@gmail.com"; //

    public SceneAnimController SceneAnimController;
    public TextMeshProUGUI CommentPlaceHolder;
    public TextMeshProUGUI TextEn;
    public TextMeshProUGUI TextRu;
    public TextMeshProUGUI TextEs;
    public TextMeshProUGUI TextButtonSupport;
    public TextMeshProUGUI TextButtonLogout;
    public TextMeshProUGUI TextButtonComment;

    public TMP_InputField CommentInput;
    public GameObject MailProgressBar;

    public Button CommentButton;

    [HideInInspector]
    public LocalRepositoryController LocalRepositoryController;

    private void Awake()
    {
        LocalRepositoryController = new LocalRepositoryController();

        string language = LocalRepositoryController.GetLanguage().ToUpper();
        switch (language)
        {
            case ("RU"):
                TextRu.gameObject.SetActive(true);
                CommentPlaceHolder.text = "Введите Ваш комментарий";
                TextButtonSupport.text = "Поддержать";
                TextButtonLogout.text = "Закончить";
                TextButtonComment.text = "Комментировать";
                break;
            case ("EN"):
                TextEn.gameObject.SetActive(true);
                CommentPlaceHolder.text = "Your Comments";
                TextButtonSupport.text = "Support us";
                TextButtonLogout.text = "Exit";
                TextButtonComment.text = "Post";
                break;
            case ("ES"):
                TextEs.gameObject.SetActive(true);
                CommentPlaceHolder.text = "Tus comentarios";
                TextButtonSupport.text = "Apóyanos";
                TextButtonLogout.text = "Salir";
                TextButtonComment.text = "Enviar";
                break;
        }
    }

    public void SendEmail()
    {
        string emailSubject = System.Uri.EscapeUriString("Bugaddy Support");
        string emailBody = System.Uri.EscapeUriString(CommentInput.text);
        Application.OpenURL("mailto:" + EMAIL + "?subject=" + emailSubject + "&body=" + emailBody);
    }

    public void SendMail()
    {
        string email = LocalRepositoryController.GetUser().ToLower();

        //var client = new SmtpClient("smtp.mail.ru", 25)
        var client = new SmtpClient("smtp.gmail.com", 25)

        {
            //Credentials = new NetworkCredential("bugaddy@mail.ru", "gaddy2021"),
            Credentials = new NetworkCredential("impossiblepossibleapps@gmail.com", "IPP2021Apps"),
            EnableSsl = true,
            Timeout = 12000
        };

        ServicePointManager.ServerCertificateValidationCallback =
        delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) {
            return true;
        };

        string body = "Пользователь " + email + " оставил комментарий:" + "\n\n" + CommentInput.text;

        //client.Send("bugaddy@mail.ru", "ponomarevigor1995@gmail.com", "Bugaddy Login", body);

        if (CommentInput.text.Length > 0)
        {
            CommentButton.interactable = false;
            MailProgressBar.gameObject.SetActive(true);
            client.SendCompleted += new SendCompletedEventHandler(SendCompletedCallback);
            try
            {
                StartCoroutine(CheckMailSuccess(client));
                //client.Send("bugaddy@mail.ru", "ponomarevigor1995@gmail.com", "Bugaddy Login", body);
                client.SendMailAsync("bugaddy@mail.ru", EMAIL, "Bugaddy Comment", body);
                //client.SendMailAsync("bugaddy@mail.ru", "bugaddy@mail.ru", "Bugaddy Comment", body);
            }
            catch
            {
                MailProgressBar.gameObject.SetActive(false);
                CommentButton.interactable = true;
            }
        }
    }

    public IEnumerator CheckMailSuccess(SmtpClient client)
    {
        yield return new WaitForSeconds(10);
        client.SendAsyncCancel();
        MailProgressBar.gameObject.SetActive(false);
        CommentButton.interactable = true;
    }

    private void SendCompletedCallback(object sender, AsyncCompletedEventArgs e)
    {
        if (e.Cancelled)
        {
        }
        if (e.Error != null)
        {
        }
        else
        {
        }
        MailProgressBar.gameObject.SetActive(false);
        CommentButton.interactable = true;
    }

    public void Exit()
    {
        Application.Quit();
    }

    public void OpenSupportUrl()
    {
        string language = LocalRepositoryController.GetLanguage().ToUpper();
        string url = "";
        switch (language)
        {
            case ("RU"):
                //url = "https://www.paypal.com/donate?hosted_button_id=ZT78F5A4Q9UYW";
                url = "https://www.bugaddy.com/ru.html#supportbugaddy";
                break;
            case ("EN"):
                //url = "https://www.paypal.com/donate?hosted_button_id=2QHFET9ZUXJWJ";
                url = "https://www.bugaddy.com/#supportbugaddy";
                break;
            case ("ES"):
                //url = "https://www.paypal.com/donate?hosted_button_id=PPLWR32VFRARQ";
                url = "https://www.bugaddy.com/es.html#supportbugaddy";
                break;
        }
        Application.OpenURL(url);
    }
}
