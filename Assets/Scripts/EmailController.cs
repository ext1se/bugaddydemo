﻿using Michsky.UI.ModernUIPack;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class EmailController : MonoBehaviour
{
    public const string EMAIL = "info@bugaddy.com"; //
    //public const string EMAIL = "ponomarevigor1995@gmail.com"; //


    public SceneAnimController SceneAnimController;
    public TMP_InputField EmailInput;
    public Button StartButton;

    public ModalWindowManager RuWindow;
    public ModalWindowManager EsWindow;
    public ModalWindowManager EnWindow;

    [HideInInspector]
    public LocalRepositoryController LocalRepositoryController;

    private void Awake()
    {
        LocalRepositoryController = new LocalRepositoryController();

        string email = LocalRepositoryController.GetUser();
        if (email.Length > 0)
        {
            EmailInput.text = email;
            StartButton.interactable = true;
        }
        else
        {
            StartButton.interactable = false;
        }

        EmailInput.onValueChanged.AddListener(delegate { ValueChangeCheck(); });
    }

    public void ValueChangeCheck()
    {
        if (EmailInput.text.Length > 0)
        {
            StartButton.interactable = true;
        }
        else
        {
            StartButton.interactable = false;
        }
    }

    public void GoNextScene(string name = "BugaddyAR")
    {
        StartButton.interactable = false;
        LocalRepositoryController.SaveUser(EmailInput.text);
         
        //var client = new SmtpClient("smtp.mail.ru", 465)
        var client = new SmtpClient("smtp.gmail.com", 25)
        {
            //Credentials = new NetworkCredential("bugaddy@mail.ru", "gaddy2021"),
            Credentials = new NetworkCredential("impossiblepossibleapps@gmail.com", "IPP2021Apps"),
            EnableSsl = true,
            Timeout = 12000
        };

        ServicePointManager.ServerCertificateValidationCallback =
            delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) {
                return true;
            };

        string body = "Пользователь " + EmailInput.text.ToLower() + " вошел в систему.";
        client.SendCompleted += new SendCompletedEventHandler(SendCompletedCallback);
        try
        {
            StartCoroutine(CheckMailSuccess(client));
            //client.Send("bugaddy@mail.ru", "ponomarevigor1995@gmail.com", "Bugaddy Login", body);
            client.SendMailAsync("bugaddy@mail.ru", EMAIL, "Bugaddy Login", body);
            //client.SendMailAsync("bugaddy@mail.ru", "bugaddy@mail.ru", "Bugaddy Login", body);
        }
        catch
        {
            LoadNextScene();
        }
        //client.Send("bugaddy@mail.ru", EMAIL, "Bugaddy Login", body);
        //SceneAnimController.LoadScene(name);
    }

    public IEnumerator CheckMailSuccess(SmtpClient client)
    {
        yield return new WaitForSeconds(10);
        client.SendAsyncCancel();
        LoadNextScene();
    }

    private void SendCompletedCallback(object sender, AsyncCompletedEventArgs e)
    {
        if (e.Cancelled)
        {
        }
        if (e.Error != null)
        {
        }
        else
        {
        }
        LoadNextScene();
    }

    private void LoadNextScene()
    {
        StartButton.interactable = true;


        string language = LocalRepositoryController.GetLanguage().ToUpper();
        switch (language)
        {
            case ("RU"):
                RuWindow.OpenWindow();
                break;
            case ("EN"):
                EnWindow.OpenWindow();
                break;
            case ("ES"):
                EsWindow.OpenWindow();
                break;
        }

        //UnityMainThreadDispatcher.Instance().Enqueue(()=>{
        //SceneAnimController.LoadScene("BugaddyAR");
        //});
    }

    public void OpenUrl()
    {
        Application.OpenURL("https://www.bugaddy.com");
    }
}
