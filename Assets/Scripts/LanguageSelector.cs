﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LanguageSelector : MonoBehaviour
{
    public EmailController EmailController;

    public GameObject EnButton;
    public GameObject RusButton;
    public GameObject EsButton;

    public TextMeshProUGUI TextButtonStart;

    private void Start()
    {
        Clear();

        string language = EmailController.LocalRepositoryController.GetLanguage().ToUpper();
        switch (language)
        {
            case ("RU"):
                ClickButton(1);
                break;
            case ("EN"):
                ClickButton(0);
                break;
            case ("ES"):
                ClickButton(2);
                break;
        }
    }

    private void Update()
    {

    }

    public void ClickButton(int position)
    {
        Clear();
        Debug.Log("pos = " + position);
        switch (position)
        {
            case (0):
                TextButtonStart.text = "Start";
                EnButton.GetComponent<Image>().color = Color.white;
                EmailController.LocalRepositoryController.SaveLanguage("EN");
                break;
            case (1):
                TextButtonStart.text = "Начать";
                RusButton.GetComponent<Image>().color = Color.white;
                EmailController.LocalRepositoryController.SaveLanguage("RU");
                break;
            case (2):
                TextButtonStart.text = "Empezar";
                EsButton.GetComponent<Image>().color = Color.white;
                EmailController.LocalRepositoryController.SaveLanguage("ES");
                break;
        }
    }

    private void Clear()
    {
        EnButton.GetComponent<Image>().color = Color.clear;
        RusButton.GetComponent<Image>().color = Color.clear;
        EsButton.GetComponent<Image>().color = Color.clear;
    }
}
