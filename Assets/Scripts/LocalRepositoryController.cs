using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalRepositoryController
{
    public const string KEY_SCORE = "KEY_SCORE";
    public const string KEY_USER = "KEY_USER";
    public const string KEY_LANGUAGE = "KEY_LANGUAGE";

    public void SaveLanguage(string language)
    {
        PlayerPrefs.SetString(KEY_LANGUAGE, language);
    }

    public string GetLanguage()
    {
        return PlayerPrefs.GetString(KEY_LANGUAGE, "RU");
    }

    public void SaveUser(string email)
    {
        PlayerPrefs.SetString(KEY_USER, email);
    }

    public string GetUser()
    {
        return PlayerPrefs.GetString(KEY_USER, "");
    }
}
