﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneAnimController : MonoBehaviour
{
    public Animator TransitionAnimator;
    public float Delay = 0.5f;
    public bool IsFirstScene = false;
    public string FirstSceneName = "EmailScene";

    public bool UseStartAnim = true;

    private void Start()
    {
        if (UseStartAnim)
        {
            TransitionAnimator.SetTrigger("start");
        }
        if (IsFirstScene)
        {
            StartCoroutine(LoadFirstSceneAsync());
        }
    }

    public void LoadScene(string name)
    {
        StartCoroutine(LoadSceneAsync(name));
    }

    private IEnumerator LoadSceneAsync(string name)
    {
        TransitionAnimator.SetTrigger("end");
        yield return new WaitForSeconds(Delay);
        SceneManager.LoadSceneAsync(name);
    }

    private IEnumerator LoadFirstSceneAsync()
    {
        yield return new WaitForSeconds(3.0f);
        TransitionAnimator.SetTrigger("end");
        yield return new WaitForSeconds(Delay);
        SceneManager.LoadScene(FirstSceneName);
    }

    public void OpenUrl()
    {
        Application.OpenURL("https://www.bugaddy.com");
    }
}
