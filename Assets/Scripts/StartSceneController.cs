﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class StartSceneController : MonoBehaviour
{
    public SceneAnimController SceneAnimController;
    public TextMeshProUGUI TextButtonStart;

    [HideInInspector]
    public LocalRepositoryController LocalRepositoryController;

    private void Awake()
    {
        LocalRepositoryController = new LocalRepositoryController();

        string language = LocalRepositoryController.GetLanguage().ToUpper();
        switch (language)
        {
            case ("RU"):
                TextButtonStart.text = "Начать";
                break;
            case ("EN"):
                TextButtonStart.text = "Start";
                break;
            case ("ES"):
                TextButtonStart.text = "Empezar";
                break;
        }
    }
}
